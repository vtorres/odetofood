﻿using Microsoft.AspNetCore.Mvc;

namespace OdeToFood.Controllers
{
    [Route("[controller]/[action]/")]
    public class AboutController
    {
        public string Phone()
        {
            return "1323213";
        }

        public string Address()
        {
            return "UK";
        }
    }
}