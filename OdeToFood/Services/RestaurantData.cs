﻿using System.Collections.Generic;
using System.Linq;
using OdeToFood.Entities;

namespace OdeToFood.Services
{
    public interface IRestaurantData
    {
        IEnumerable<Restaurant> GetAll();
        Restaurant Get(int id);
        Restaurant Add(Restaurant newRestaurant);
        void Commit();
    }

    public class SqlRestaurantData : IRestaurantData
    {
        private OdeToFoodDBContext _context;

        public SqlRestaurantData(OdeToFoodDBContext context)
        {
            _context = context;
        }

        Restaurant IRestaurantData.Add(Restaurant newRestaurant)
        {
            _context.Add(newRestaurant);
            _context.SaveChanges();
            return newRestaurant;
        }

        void IRestaurantData.Commit()
        {
            _context.SaveChanges();
        }

        Restaurant IRestaurantData.Get(int id)
        {
            return _context.Restaurants.FirstOrDefault(r => r.Id == id);
        }

        IEnumerable<Restaurant> IRestaurantData.GetAll()
        {
            return _context.Restaurants;
        }
    }

    public class InMemoryRestaurantData : IRestaurantData
    {
        static InMemoryRestaurantData()
        {
            _restaurants = new List<Restaurant>
            {
                new Restaurant {Id = 1, Name = "Restaurant 1"},
                new Restaurant {Id = 2, Name = "Restaurant 2"},
                new Restaurant {Id = 3, Name = "Restaurant 3"}
            };
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return _restaurants;
        }

        public Restaurant Get(int id)
        {
            return _restaurants.FirstOrDefault(r => r.Id == id);
        }

        public Restaurant Add(Restaurant newRestaurant)
        {
            newRestaurant.Id = _restaurants.Max(r => r.Id) + 1;
            _restaurants.Add(newRestaurant);
            return newRestaurant;
        }

        public void Commit()
        {
            // no-op
            throw new System.NotImplementedException();
        }

        private static List<Restaurant> _restaurants;
    }
}