using Microsoft.EntityFrameworkCore;
using OdeToFood.Entities;

namespace OdeToFood.Entities
{
    public class OdeToFoodDBContext : DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }

        public OdeToFoodDBContext(DbContextOptions options) : base(options)
        {
            
        }
    }
}